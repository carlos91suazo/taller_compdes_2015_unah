# README #
[Taller COMPDES 2015] Introducción al Desarrollo de videojuegos 2d y 3d con Unity 3D.

### Descripción ###

Este es el repositorio del proyecto usado en el  taller de Unity3D para el COMPDES 2015 UNAH
### Instrucciones ###

* Clonar el repositorio en un lugar accesible.
* Proyecto hecho con Unity3d Versión 4.6xx.
* Si se usa una versión posterior de Unity3D puedes actualizar los scripts con toda confianza.
* Este repositorio trae una escena ya terminada por si quieren ver el juego final.

### Licencia ###

* Versión en español del proyecto de ejemplo "Survival Shooter" de la página oficial de Unity3D.
* Versión original: http://unity3d.com/es/learn/tutorials/projects/survival-shooter.
* Proyecto usado con fines educativos No comerciales. 
