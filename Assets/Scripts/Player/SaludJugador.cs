﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SaludJugador : MonoBehaviour
{
    public int saludInicial = 100;
    public int saludActual;
    public Slider barraSalud;
    public Image imagenDanio;
    public AudioClip audioMuerte;
    public float velocidadDestello = 5f;
    public Color colorDestello = new Color(1f, 0f, 0f, 0.1f);


    Animator anim;
    AudioSource audioJugador;
    MovimientoJugador movimientoJugador;
    //DisparoDelJugador disparoDelJugador;
    bool estaMuerto;
    bool daniado;


    void Awake ()
    {
        anim = GetComponent <Animator> ();
        audioJugador = GetComponent <AudioSource> ();
        movimientoJugador = GetComponent <MovimientoJugador> ();
        //disparoDelJugador = GetComponentInChildren <DisparoDelJugador> ();
        saludActual = saludInicial;
    }


    void Update ()
    {
        if(daniado)
        {
            imagenDanio.color = colorDestello;
        }
        else
        {
            imagenDanio.color = Color.Lerp (imagenDanio.color, Color.clear, velocidadDestello * Time.deltaTime);
        }
        daniado = false;
    }


    public void TomarDanios (int cantidad)
    {
        daniado = true;

        saludActual -= cantidad;

        barraSalud.value = saludActual;

        audioJugador.Play ();

        if(saludActual <= 0 && !estaMuerto)
        {
            Muerte ();
        }
    }


    void Muerte ()
    {
        estaMuerto = true;

        //disparoDelJugador.DesactivarEfectos ();

        anim.SetTrigger ("Die");

        audioJugador.clip = audioMuerte;
        audioJugador.Play ();

        movimientoJugador.enabled = false;
        //disparoDelJugador.enabled = false;
    }
}
