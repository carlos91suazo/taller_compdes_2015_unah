﻿using UnityEngine;

public class DisparoDelJugador : MonoBehaviour
{
    public int danioPorDisparo = 20;
	public float tiempoEntreBalas = 0.15f;
    public float rango = 100f;


    float timer;
    Ray rayDisparo;
    RaycastHit golpeDisparo;
    int maskDisparable;
    ParticleSystem gunParticles;
    LineRenderer lineaDisparo;
    AudioSource audioArma;
    Light luzArma;
	float tiempoMostrarEfectos = 0.2f;


    void Awake ()
    {
        maskDisparable = LayerMask.GetMask ("Shootable");
        gunParticles = GetComponent<ParticleSystem> ();
        lineaDisparo = GetComponent <LineRenderer> ();
        audioArma = GetComponent<AudioSource> ();
        luzArma = GetComponent<Light> ();
    }


    void Update ()
    {
        timer += Time.deltaTime;

        if(Input.GetButton ("Fire1") && timer >= tiempoEntreBalas)
        {
            Disparo ();
        }

        if(timer >= tiempoEntreBalas * tiempoMostrarEfectos)
        {
            DesactivarEfectos ();
        }
    }


    public void DesactivarEfectos ()
    {
        lineaDisparo.enabled = false;
        luzArma.enabled = false;
    }


    void Disparo ()
    {
        timer = 0f;

        audioArma.Play ();

        luzArma.enabled = true;

        gunParticles.Stop ();
        gunParticles.Play ();

        lineaDisparo.enabled = true;
        lineaDisparo.SetPosition (0, transform.position);

        rayDisparo.origin = transform.position;
        rayDisparo.direction = transform.forward;

        if(Physics.Raycast (rayDisparo, out golpeDisparo, rango, maskDisparable))
        {
            SaludEnemigo saludEnemigo = golpeDisparo.collider.GetComponent <SaludEnemigo> ();
            if(saludEnemigo != null)
            {
                saludEnemigo.TomarDanios (danioPorDisparo, golpeDisparo.point);
            }
            lineaDisparo.SetPosition (1, golpeDisparo.point);
        }
        else
        {
            lineaDisparo.SetPosition (1, rayDisparo.origin + rayDisparo.direction * rango);
        }
    }
}
