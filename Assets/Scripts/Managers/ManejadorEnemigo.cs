﻿using UnityEngine;

public class ManejadorEnemigo : MonoBehaviour
{
    public SaludJugador saludJugador;
    public GameObject enemigo;
    public float tiempoNacer = 3f;
    public Transform[] spawnPoints;


    void Start ()
    {
        InvokeRepeating ("Spawn", tiempoNacer, tiempoNacer);
    }


    void Spawn ()
    {
        if(saludJugador.saludActual <= 0f)
        {
            return;
        }

        int spawnPointIndex = Random.Range (0, spawnPoints.Length);

        Instantiate (enemigo, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);
    }
}
