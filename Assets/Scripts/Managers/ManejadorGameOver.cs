﻿using UnityEngine;

public class ManejadorGameOver : MonoBehaviour
{
    public SaludJugador saludJugador;
    public float retardarInicio = 5f;


    Animator anim;
    float timerInicio;


    void Awake()
    {
        anim = GetComponent<Animator>();
    }


    void Update()
    {
        if (saludJugador.saludActual <= 0)
        {
            anim.SetTrigger("GameOver");

            timerInicio += Time.deltaTime;

            if (timerInicio >= retardarInicio)
            {
                Application.LoadLevel(Application.loadedLevel);
            }
        }
    }
}
