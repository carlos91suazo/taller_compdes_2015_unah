﻿using UnityEngine;
using System.Collections;

public class AtaqueEnemigo : MonoBehaviour
{
    public float tiempoEntreAtaques = 0.5f;
    public int danioAtaque = 10;


    Animator anim;
    GameObject jugador;
    SaludJugador saludJugador;
    //SaludEnemigo saludEnemigo;
    bool jugadorEnRango;
    float timer;


    void Awake ()
    {
        jugador = GameObject.FindGameObjectWithTag ("Player");
        saludJugador = jugador.GetComponent <SaludJugador> ();
        //saludEnemigo = GetComponent<SaludEnemigo>();
        anim = GetComponent <Animator> ();
    }


    void OnTriggerEnter (Collider other)
    {
        if(other.gameObject == jugador)
        {
            jugadorEnRango = true;
        }
    }


    void OnTriggerExit (Collider other)
    {
        if(other.gameObject == jugador)
        {
            jugadorEnRango = false;
        }
    }


    void Update ()
    {
        timer += Time.deltaTime;

        if(timer >= tiempoEntreAtaques && jugadorEnRango /*&& saludEnemigo.saludActual > 0*/)
        {
            Attack ();
        }

        if(saludJugador.saludActual <= 0)
        {
            anim.SetTrigger ("PlayerDead");
        }
    }


    void Attack ()
    {
        timer = 0f;

        if(saludJugador.saludActual > 0)
        {
            saludJugador.TomarDanios (danioAtaque);
        }
    }
}
