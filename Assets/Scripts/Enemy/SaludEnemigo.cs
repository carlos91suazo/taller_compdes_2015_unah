﻿using UnityEngine;

public class SaludEnemigo : MonoBehaviour
{
    public int saludInicial = 100;
    public int saludActual;
    public float sinkSpeed = 2.5f;
    public int scoreValue = 10;
    public AudioClip audioMuerte;


    Animator anim;
    AudioSource audioEnemigo;
    ParticleSystem particulasDanio;
    CapsuleCollider capsuleCollider;
    bool estaMuerto;
    bool seEstaHundiendo;


    void Awake ()
    {
        anim = GetComponent <Animator> ();
        audioEnemigo = GetComponent <AudioSource> ();
        particulasDanio = GetComponentInChildren <ParticleSystem> ();
        capsuleCollider = GetComponent <CapsuleCollider> ();

        saludActual = saludInicial;
    }


    void Update ()
    {
        if(seEstaHundiendo)
        {
            transform.Translate (-Vector3.up * sinkSpeed * Time.deltaTime);
        }
    }


    public void TomarDanios (int cantidad, Vector3 puntoGolpe)
    {
        if(estaMuerto)
            return;

        audioEnemigo.Play ();

        saludActual -= cantidad;
            
        particulasDanio.transform.position = puntoGolpe;
        particulasDanio.Play();

        if(saludActual <= 0)
        {
            Muerte ();
        }
    }


    void Muerte ()
    {
        estaMuerto = true;

        capsuleCollider.isTrigger = true;

        anim.SetTrigger ("Dead");

        audioEnemigo.clip = audioMuerte;
        audioEnemigo.Play ();
    }


    public void StartSinking ()
    {
        GetComponent <NavMeshAgent> ().enabled = false;
        GetComponent <Rigidbody> ().isKinematic = true;
        seEstaHundiendo = true;
        //ManejadorPuntuacion.score += scoreValue;
        Destroy (gameObject, 2f);
    }
}
